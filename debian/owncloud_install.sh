#!/bin/bash

# Tech and Me, ©2016 - www.techandme.se
# Code 42 - www.code42.fr

# Debian 8.X Jessie

export CONVER=v1.2.0.0
export CONVER_FILE=contacts.tar.gz
export CALVER=v1.1
export CALVER_FILE=calendar.tar.gz
export OCDATA=/var/ocdata
export MYSQL_VERSION=5.7
export SHUF=$(shuf -i 13-15 -n 1)
export MYSQL_PASS=$(cat /dev/urandom | tr -dc "a-zA-Z0-9@#*=" | fold -w $SHUF | head -n 1)
export ROOT_PASS=$(cat /dev/urandom | tr -dc "a-zA-Z0-9@#*=" | fold -w $SHUF | head -n 1)
export PW_FILE=/M-R_passwords.txt
export SCRIPTS=/opt/code42/scripts/owncloud
export LOG_FOLDER=/opt/code42/log/owncloud
export HTML=/var/www
export OCPATH=$HTML/owncloud
export SSL_CONF="/etc/apache2/sites-available/owncloud_ssl_domain_self_signed.conf"
export IP="/sbin/ip"
export IFACE=$($IP -o link show | awk '{print $2,$9}' | grep "UP" | cut -d ":" -f 1)
export ADDRESS=$(hostname -I | cut -d ' ' -f 1)

export REPO_OWNCLOUD="http://download.owncloud.org/download/repositories/stable/Debian_8.0"
export REPO_OWNCLOUD_KEY="$REPO_OWNCLOUD/Release.key"
export REPO_OWNCLOUD_INSTALL="https://raw.githubusercontent.com/enoch85/ownCloud-VM/master/static"
export REPO=https://github.com/owncloud
export CALVER_REPO=$REPO/calendar/releases/download
export CONVER_REPO=$REPO/contacts/releases/download

mkdir -p ${LOG_FOLDER}
LOG_FILE=$(mktemp ${LOG_FOLDER}/Owncloud.XXXXX.log)

echo "#0 Check if root" >> ${LOG_FILE} 2>&1
        if [ "$(whoami)" != "root" ]; then
        echo
        echo -e "\e[31mSorry, you are not root.\n\e[0mYou must type: \e[36msu root -c 'bash ${SCRIPTS}/owncloud_install.sh'"
        echo
        exit 1
fi

echo "#1 Aptitude upgrade"  >> ${LOG_FILE} 2>&1
aptitude upgrade -y >> ${LOG_FILE} 2>&1

echo "#2 Check if repo is availible" >> ${LOG_FILE} 2>&1
if wget -q --spider ${REPO_OWNCLOUD}
    then
        echo "ownCloud repo OK" >> ${LOG_FILE} 2>&1
else
        echo "ownCloud repo is not availible, exiting..." >> ${LOG_FILE} 2>&1
        exit 1
fi

if wget -q --spider ${REPO_OWNCLOUD_KEY}
    then
        echo "ownCloud repo key OK" >> ${LOG_FILE} 2>&1
else
        echo "ownCloud repo key is not availible, exiting..." >> ${LOG_FILE} 2>&1
        exit 1
fi

echo "#3 Check if it's a clean server" >> ${LOG_FILE} 2>&1
if dpkg --list mysql-common | egrep -q ^ii
    then
        echo "MySQL is installed, it must be a clean server." >> ${LOG_FILE} 2>&1
        exit 1
fi

if dpkg --list apache2 | egrep -q ^ii
    then
        echo "Apache2 is installed, it must be a clean server." >> ${LOG_FILE} 2>&1
        exit 1
fi

if dpkg --list php | egrep -q ^ii
        then
        echo "PHP is installed, it must be a clean server." >> ${LOG_FILE} 2>&1
        exit 1
fi

if dpkg --list owncloud | egrep -q ^ii
    then
        echo "ownCloud is installed, it must be a clean server." >> ${LOG_FILE} 2>&1
        exit 1
fi

echo "#4 Change DNS" >> ${LOG_FILE} 2>&1
echo "nameserver 8.8.8.8" > /etc/resolv.conf
echo "nameserver 8.8.4.4" >> /etc/resolv.conf

echo "#5 Check network" >> ${LOG_FILE} 2>&1
ifdown $IFACE && ifup $IFACE  >> ${LOG_FILE} 2>&1
dig +short google.com  >> ${LOG_FILE} 2>&1
if [[ $? > 0 ]]
    then
    echo "Network NOT OK. You must have a working Network connection to run this script." >> ${LOG_FILE} 2>&1
    exit
else
    echo "Network OK." >> ${LOG_FILE} 2>&1
fi

echo "#6 Update system" >> ${LOG_FILE} 2>&1
aptitude update >> ${LOG_FILE} 2>&1

echo "#7.1 Install Sudo" >> ${LOG_FILE} 2>&1
aptitude install sudo -y >> ${LOG_FILE} 2>&1
adduser ocadmin sudo >> ${LOG_FILE} 2>&1

echo "#7.2 Install Rsync" >> ${LOG_FILE} 2>&1
aptitude install rsync -y >> ${LOG_FILE} 2>&1

echo "#7.3 Install figlet for techandme.sh" >> ${LOG_FILE} 2>&1
aptitude install figlet -y >> ${LOG_FILE} 2>&1

echo "#8 Set locales & timezone to Swedish" >> ${LOG_FILE} 2>&1
echo "Europe/Paris" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales  >> ${LOG_FILE} 2>&1

echo "#9 Set Random passwords" >> ${LOG_FILE} 2>&1
echo
echo "The MySQL password will now be set..."
echo
sleep 2
echo -e "Your MySQL root password is: \e[32m$MYSQL_PASS\e[0m"
echo "Please save this somewhere safe. You can not login to MySQL without it." 
echo "The password is also saved in this file: $PW_FILE."
echo "MySQL password: $MYSQL_PASS" > $PW_FILE
echo -e "\e[32m"
read -p "Press any key to continue..." -n1 -s
echo -e "\e[0m"
echo
echo "The ROOT password will now change..."
echo
sleep 2
echo -e "root:$ROOT_PASS" | chpasswd
echo -e "Your new ROOT password is: \e[32m$ROOT_PASS\e[0m"
echo "Please save this somewhere safe. You can not login as root without it."
echo "The password is also saved in this file: $PW_FILE."
echo "ROOT password: $ROOT_PASS" >> $PW_FILE
chmod 600 $PW_FILE
echo -e "\e[32m"
read -p "Press any key to continue..." -n1 -s
echo -e "\e[0m"

echo "#10.1 Install MYSQL 5.7" >> ${LOG_FILE} 2>&1
aptitude install debconf-utils -y >> ${LOG_FILE} 2>&1
if [ -d /var/lib/mysql ];
then
rm -R /var/lib/mysql
fi
echo "deb http://repo.mysql.com/apt/debian/ jessie mysql-$MYSQL_VERSION" > /etc/apt/sources.list.d/mysql.list
echo "deb-src http://repo.mysql.com/apt/debian/ jessie mysql-$MYSQL_VERSION" >> /etc/apt/sources.list.d/mysql.list
echo mysql-community-server mysql-community-server/root-pass password $MYSQL_PASS | debconf-set-selections
echo mysql-community-server mysql-community-server/re-root-pass password $MYSQL_PASS | debconf-set-selections
apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys A4A9406876FCBD3C456770C88C718D3B5072E1F5
aptitude update >> ${LOG_FILE} 2>&1
aptitude install mysql-community-server -y >> ${LOG_FILE} 2>&1

echo "#10.2 mysql_secure_installation" >> ${LOG_FILE} 2>&1
aptitude -y install expect >> ${LOG_FILE} 2>&1
export SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root:\"
send \"$MYSQL_PASS\r\"
expect \"Would you like to setup VALIDATE PASSWORD plugin?\"
send \"n\r\"
expect \"Change the password for root ?\"
send \"n\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")
echo "$SECURE_MYSQL" >> ${LOG_FILE} 2>&1
unset SECURE_MYSQL >> ${LOG_FILE} 2>&1
aptitude -y purge expect >> ${LOG_FILE} 2>&1

echo "#11 Install Apache" >> ${LOG_FILE} 2>&1
aptitude install libc6 -y
aptitude install apache2 -y
a2enmod rewrite \
        headers \
        env \
        dir \
        mime \
        ssl \
        setenvif  >> ${LOG_FILE} 2>&1

echo "#12 Set hostname and ServerName" >> ${LOG_FILE} 2>&1
sudo sh -c "echo 'ServerName owncloud' >> /etc/apache2/apache2.conf"
sudo hostnamectl set-hostname owncloud
service apache2 restart

echo "#13 Install PHP 7" >> ${LOG_FILE} 2>&1
echo "Install PHP 7" >> ${LOG_FILE} 2>&1

echo "deb http://packages.dotdeb.org stable all" > /etc/apt/sources.list.d/dotdeb.list
echo "deb-src http://packages.dotdeb.org stable all" >> /etc/apt/sources.list.d/dotdeb.list
wget https://www.dotdeb.org/dotdeb.gpg

apt-key add dotdeb.gpg
rm dotdeb.gpg
aptitude update
aptitude install -y \
        software-properties-common \
        php7.0 \
        php7.0-common \
        php7.0-mysql \
        php7.0-intl \
        php7.0-mcrypt \
        php7.0-ldap \
        php7.0-imap \
        php7.0-cli \
        php7.0-gd \
        php7.0-pgsql \
        php7.0-json \
        php7.0-sqlite3 \
        php7.0-curl \
        php7.0-xmlrpc \
        libsm6 \
        libsmbclient  >> ${LOG_FILE} 2>&1

echo "#14.1 Download and install ownCloud" >> ${LOG_FILE} 2>&1
wget -nv $REPO_OWNCLOUD_KEY -O Release.key >> ${LOG_FILE} 2>&1
apt-key add - < Release.key && rm Release.key >> ${LOG_FILE} 2>&1
sh -c "echo 'deb $REPO_OWNCLOUD/ /' > /etc/apt/sources.list.d/owncloud.list" >> ${LOG_FILE} 2>&1
aptitude update -q2 && aptitude install owncloud-files -y >> ${LOG_FILE} 2>&1

echo "#14.2 Create data folder, occ complains otherwise" >> ${LOG_FILE} 2>&1
mkdir -p $OCDATA >> ${LOG_FILE} 2>&1

echo "#14.3 Download SCRIPTS - Secure permissions" >> ${LOG_FILE} 2>&1
wget ${REPO_OWNCLOUD_INSTALL}/setup_secure_permissions_owncloud.sh -P ${SCRIPTS} >> ${LOG_FILE} 2>&1
wget ${REPO_OWNCLOUD_INSTALL}/owncloud-startup-script.sh -P ${SCRIPTS} >> ${LOG_FILE} 2>&1
chmod +x ${SCRIPTS}/setup_secure_permissions_owncloud.sh >> ${LOG_FILE} 2>&1
chmod +x ${SCRIPTS}/owncloud-startup-script.sh >> ${LOG_FILE} 2>&1
bash ${SCRIPTS}/setup_secure_permissions_owncloud.sh >> ${LOG_FILE} 2>&1
bash ${SCRIPTS}/owncloud-startup-script.sh >> ${LOG_FILE} 2>&1

echo "#14.4 Install ownCloud" >> ${LOG_FILE} 2>&1
cd $OCPATH
su -s /bin/sh -c 'php occ maintenance:install --data-dir "$OCDATA" --database "mysql" --database-name "owncloud_db" --database-user "root" --database-pass "$MYSQL_PASS" --admin-user "ocadmin" --admin-pass "owncloud"' www-data  >> ${LOG_FILE} 2>&1
echo
echo "ownCloud version:"
su -s /bin/sh -c 'php $OCPATH/occ status' www-data
echo
sleep 3

echo "#14.5 Set trusted domain" >> ${LOG_FILE} 2>&1
wget ${REPO_OWNCLOUD_INSTALL}/update-config.php -P ${SCRIPTS} >> ${LOG_FILE} 2>&1
chmod a+x ${SCRIPTS}/update-config.php >> ${LOG_FILE} 2>&1
php ${SCRIPTS}/update-config.php $OCPATH/config/config.php 'trusted_domains[]' localhost ${ADDRESS[@]} $(hostname) $(hostname --fqdn) 2>&1 >/dev/null
php ${SCRIPTS}/update-config.php $OCPATH/config/config.php overwrite.cli.url https://$ADDRESS/owncloud 2>&1 >/dev/null

echo "#14.6 Prepare cron.php to be run every 15 minutes" >> ${LOG_FILE} 2>&1
crontab -u www-data -l | { cat; echo "*/15  *  *  *  * php -f $OCPATH/cron.php 2>&1"; } >> ${LOG_FILE} 2>&1

echo "#15.1 Change values in php.ini (increase max file size)" >> ${LOG_FILE} 2>&1
# max_execution_time
sed -i "s|max_execution_time = 30|max_execution_time = 3500|g" /etc/php/7.0/apache2/php.ini
# max_input_time
sed -i "s|max_input_time = 60|max_input_time = 3600|g" /etc/php/7.0/apache2/php.ini
# memory_limit
sed -i "s|memory_limit = 128M|memory_limit = 512M|g" /etc/php/7.0/apache2/php.ini
# post_max
sed -i "s|post_max_size = 8M|post_max_size = 1100M|g" /etc/php/7.0/apache2/php.ini
# upload_max
sed -i "s|upload_max_filesize = 2M|upload_max_filesize = 1000M|g" /etc/php/7.0/apache2/php.ini

echo "#14.8 Generate $SSL_CONF" >> ${LOG_FILE} 2>&1
if [ -f $SSL_CONF ];
        then
        echo "Virtual Host exists"
else
        touch "$SSL_CONF"
        cat << SSL_CREATE > "$SSL_CONF"
<VirtualHost *:443>
    Header add Strict-Transport-Security: "max-age=15768000;includeSubdomains"
    SSLEngine on
### YOUR SERVER ADDRESS ###
#    ServerAdmin admin@example.com
#    ServerName example.com
#    ServerAlias subdomain.example.com
### SETTINGS ###
    DocumentRoot $OCPATH

    <Directory $OCPATH>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
    Satisfy Any
    </Directory>

    Alias /owncloud "$OCPATH/"

    <IfModule mod_dav.c>
    Dav off
    </IfModule>

    <Directory "$OCDATA">
    # just in case if .htaccess gets disabled
    Require all denied
    </Directory>

    SetEnv HOME $OCPATH
    SetEnv HTTP_HOME $OCPATH
### LOCATION OF CERT FILES ###
    SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem
    SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
</VirtualHost>
SSL_CREATE
echo "$SSL_CONF was successfully created"
sleep 3
fi

echo "#15.2 Enable new config" >> ${LOG_FILE} 2>&1
a2ensite owncloud_ssl_domain_self_signed.conf
a2dissite default-ssl
service apache2 restart

echo "#16 Set config values" >> ${LOG_FILE} 2>&1
# Experimental apps

su -s /bin/sh -c 'php $OCPATH/occ config:system:set appstore.experimental.enabled --value="true"' www-data
# Default mail server (make this user configurable?)
su -s /bin/sh -c 'php $OCPATH/occ config:system:set mail_smtpmode --value="smtp"' www-data
su -s /bin/sh -c 'php $OCPATH/occ config:system:set mail_smtpauth --value="1"' www-data
su -s /bin/sh -c 'php $OCPATH/occ config:system:set mail_smtpport --value="25"' www-data
su -s /bin/sh -c 'php $OCPATH/occ config:system:set mail_smtphost --value="locahost"' www-data
su -s /bin/sh -c 'php $OCPATH/occ config:system:set mail_from_address --value=""' www-data
#su -s /bin/sh -c 'php $OCPATH/occ config:system:set mail_domain --value="gmail.com"' www-data
#su -s /bin/sh -c 'php $OCPATH/occ config:system:set mail_smtpsecure --value="ssl"' www-data
#su -s /bin/sh -c 'php $OCPATH/occ config:system:set mail_smtpname --value="www.en0ch.se@gmail.com"' www-data
#su -s /bin/sh -c 'php $OCPATH/occ config:system:set mail_smtppassword --value="techandme_se"' www-data

echo "#17 Install Unzip, Libreoffice Writer to be able to read MS documents." >> ${LOG_FILE} 2>&1
aptitude update
aptitude install unzip -y
aptitude install --no-install-recommends libreoffice-writer -y

echo "#18.1 Download and install Documents" >> ${LOG_FILE} 2>&1
if [ -d $OCPATH/apps/documents ]; then
sleep 1
else
wget -q $REPO/documents/archive/master.zip -P $OCPATH/apps
cd $OCPATH/apps
Unzip -q master.zip
rm master.zip
mv documents-master/ documents/
fi

echo "#18.2 Enable documents" >> ${LOG_FILE} 2>&1
if [ -d $OCPATH/apps/documents ]; then
sudo -u www-data php $OCPATH/occ app:enable documents
sudo -u www-data php $OCPATH/occ config:system:set preview_libreoffice_path --value="/usr/bin/libreoffice"
fi

echo "#18.3 Download and install Contacts" >> ${LOG_FILE} 2>&1
if [ -d $OCPATH/apps/contacts ]; then
sleep 1
else
wget -q $CONVER_REPO/$CONVER/$CONVER_FILE -P $OCPATH/apps
tar -zxf $OCPATH/apps/$CONVER_FILE -C $OCPATH/apps
cd $OCPATH/apps
rm $CONVER_FILE
fi

echo "#18.4 Enable Contacts" >> ${LOG_FILE} 2>&1
if [ -d $OCPATH/apps/contacts ]; then
sudo -u www-data php $OCPATH/occ app:enable contacts
fi

echo "#18.5 Download and install Calendar" >> ${LOG_FILE} 2>&1
if [ -d $OCPATH/apps/calendar ]; then
sleep 1
else
wget -q $CALVER_REPO/$CALVER/$CALVER_FILE -P $OCPATH/apps
tar -zxf $OCPATH/apps/$CALVER_FILE -C $OCPATH/apps
cd $OCPATH/apps
rm $CALVER_FILE
fi

echo "#18.6 Enable Calendar" >> ${LOG_FILE} 2>&1
if [ -d $OCPATH/apps/calendar ]; then
sudo -u www-data php $OCPATH/occ app:enable calendar
fi

echo "#19 Set secure permissions final (./data/.htaccess has wrong permissions otherwise)" >> ${LOG_FILE} 2>&1
bash ${SCRIPTS}/setup_secure_permissions_owncloud.sh

echo "#20 Start startup-script" >> ${LOG_FILE} 2>&1
bash ${SCRIPTS}/owncloud-startup-script.sh

echo "#21 Unset all $VARs" >> ${LOG_FILE} 2>&1
unset OCVERSION
unset MYSQL_VERSION
unset SHUF
unset MYSQL_PASS
unset PW_FILE
unset SCRIPTS
unset HTML
unset OCPATH
unset SSL_CONF
unset IFACE
unset ADDRESS
unset ROOT_PASS
unset IP
unset CONVER
unset CONVER_FILE
unset CONVER_REPO
unset CALVER
unset CALVER_FILE
unset CALVER_REPO
unset OCDATA

exit 0
