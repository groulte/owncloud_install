#!/bin/bash

apt-get remove owncloud* -y
apt-get remove mysql* -y
apt-get remove apache2* -y
apt-get remove libc6* -y
apt-get remove php* -y
apt-get autoremove -y
apt-get autoclean -y

