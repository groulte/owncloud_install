###################################################
#
# OWNCLOUD-INSTALL.SH 
# Eric G.
#
###################################################

FOLDER_BASE='/opt/code42'
FOLDER_UPDATE='${FOLDER_BASE}/UPDATE'

# La date du jour au format voulu
DATE_JOUR=$(date +%H%M-%d%m%Y)

FILE=`echo $1 | awk -F/ '{print $NF}'`
FILE_VERSION=`echo $FILE | sed -E 's/\.t(ar\.)?[b]z2$//g'`

BACKUP-DESTINATION='/var/www-${FILE_VERSION}'

### LOG - CONF ###
rm -f ${FOLDER_UPDATE}/Update.latest.log
LOG_FILE=`mktemp ${FOLDER_UPDATE}/Update.${FILE_VERSION}.X.log`
ln -s ${LOG_FILE} ${FOLDER_UPDATE}/Update.latest.log

### 0 - START ###
echo "### 0 - START - ${FILE_VERSION} - ${DATE_JOUR} ###" >> ${LOG_FILE} 2>&1

### 1 - DL ###
cd ${FOLDER_UPDATE} >> ${LOG_FILE} 2>&1
wget $1 -o ${LOG_FILE} >> ${LOG_FILE} 2>&1

### 2.1 - BACKUP FILE ###
mkdir -P /var/www-${FILE_VERSION}
rsync -Harpov --exclude ./data ./config * ../${BACKUP-DESTINATION}

### 2.2 - COMPRESS FILE ###
## TO DONE ##

### 2.3 - RM FILE ###
cd ${FOLDER_BASE} >> ${LOG_FILE} 2>&1
find . -mindepth 1 -maxdepth 1 ! -path ./data ! -path ./config ! -path ${FOLDER_UPDATE} -exec rm -rf {} \; >> ${LOG_FILE} 2>&1

### 3 - UNZIP ###
tar -xvjf ${FOLDER_UPDATE}/${FILE} ../ >> ${LOG_FILE} 2>&1

### 4 - CHOWN ###
chown www-data:www-data * -R >> ${LOG_FILE} 2>&1

### 10 - END ###
echo "### 10 - END ###" >> ${LOG_FILE} 2>&1